package kz.ahmadtgr.testproject.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Ahmad on 08.04.2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Builder
@Entity
@Table(name = "administrators")
public class Administrator {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "administrator", fetch = FetchType.LAZY)
    private Set<Driver> drivers;

}
