package kz.ahmadtgr.testproject.controllers;

import jdk.nashorn.internal.runtime.logging.Logger;
import kz.ahmadtgr.testproject.dtos.BusDto;
import kz.ahmadtgr.testproject.entities.Bus;
import kz.ahmadtgr.testproject.services.DataAccessService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Ahmad on 11.04.2019.
 */
@Logger
@Slf4j
@RestController
@RequestMapping("/bus")
public class BusController {

    @Autowired
    private DataAccessService service;

    private ModelMapper modelMapper = new ModelMapper();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<BusDto>> readAll() {
        List<Bus> existingEntities = service.findAllBuses();

        return ResponseEntity.ok(existingEntities
                .stream()
                .map(bus ->  modelMapper.map(bus, BusDto.class))
                .collect(Collectors.toList()));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BusDto> create(@RequestBody BusDto requestDto) {

        try {
            Bus requestEntity = modelMapper.map(requestDto, Bus.class);
            Bus saveResult = service.save(requestEntity);
            BusDto responseDto = modelMapper.map(saveResult, BusDto.class);

            return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
        } catch (Exception e2) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(method = RequestMethod.PUT, value = "{id}")
    public ResponseEntity<BusDto> update(@PathVariable("id") int id, @RequestBody BusDto requestDto) {
        Optional<Bus> existingEntity = service.findBusById(id);
        if (!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Bus requestEntity = modelMapper.map(requestDto, Bus.class);

        Bus entity = existingEntity.get();
        entity.setBusNumber(requestEntity.getBusNumber());
        Bus saveResult = service.save(entity);

        BusDto responseDto = modelMapper.map(saveResult, BusDto.class);

        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<Bus> delete(@PathVariable int id) {
        Optional<Bus> existingEntity = service.findBusById(id);
        if(!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try{
            service.delete(existingEntity.get());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
