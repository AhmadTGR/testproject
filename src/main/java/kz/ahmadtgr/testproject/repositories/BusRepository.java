package kz.ahmadtgr.testproject.repositories;

import kz.ahmadtgr.testproject.entities.Bus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Ahmad on 09.04.2019.
 */
@Repository
public interface BusRepository extends JpaRepository<Bus, Integer> {

}
