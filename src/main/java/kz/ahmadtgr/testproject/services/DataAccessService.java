package kz.ahmadtgr.testproject.services;

import kz.ahmadtgr.testproject.entities.Administrator;
import kz.ahmadtgr.testproject.entities.Bus;
import kz.ahmadtgr.testproject.entities.Driver;
import kz.ahmadtgr.testproject.repositories.AdministratorRepository;
import kz.ahmadtgr.testproject.repositories.BusRepository;
import kz.ahmadtgr.testproject.repositories.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ahmad on 09.04.2019.
 */
@Service
//@Slf4j
public class DataAccessService {

    @Autowired
    AdministratorRepository administratorRepository;

    @Autowired
    BusRepository busRepository;

    @Autowired
    DriverRepository driverRepository;

    public Optional<Administrator> findAdministratorById(int id) {
        return administratorRepository.findById(id);
    }
    public Optional<Administrator> findAdministratorByName(String name) { return administratorRepository.findByName(name); };
    public List<Administrator> findAllAdministrators() {
        return administratorRepository.findAll();
    }
    public Administrator save(Administrator administrator) {
        return administratorRepository.save(administrator);
    }
    public void delete(Administrator administrator) {
        administratorRepository.delete(administrator);
    }

    public Optional<Bus> findBusById(int id) { return busRepository.findById(id); }
    public List<Bus> findAllBuses() { return busRepository.findAll(); }
    public Bus save(Bus bus) { return busRepository.save(bus); }
    public void delete(Bus bus) { busRepository.delete(bus); }

    public Optional<Driver> findDriverById(int id) { return driverRepository.findById(id); }
    public List<Driver> findAllDrivers() { return driverRepository.findAll(); }
    public Driver save(Driver driver) { return driverRepository.save(driver); }
    public void delete(Driver driver) { driverRepository.delete(driver); }

}
