package kz.ahmadtgr.testproject.dtos;

import lombok.*;

/**
 * Created by Ahmad on 09.04.2019.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class DriverDto {

    private int id;
    private String name;
    private int administratorId;
    private int busId;
}
