package kz.ahmadtgr.testproject.controllers;


import jdk.nashorn.internal.runtime.logging.Logger;
import kz.ahmadtgr.testproject.exceptions.NoSuchEntityException;
import kz.ahmadtgr.testproject.dtos.AdministratorDto;
import kz.ahmadtgr.testproject.entities.Administrator;
import kz.ahmadtgr.testproject.services.DataAccessService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Ahmad on 09.04.2019.
 */
@Logger
@Slf4j
@RestController
@RequestMapping("/administrator")
public class AdministratorController {


    @Autowired
    private DataAccessService service;

    private ModelMapper modelMapper = new ModelMapper();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<AdministratorDto>> readAll() {
        List<Administrator> existingEntities = service.findAllAdministrators();

        return ResponseEntity.ok(existingEntities
                .stream()
                .map(administrator ->  modelMapper.map(administrator, AdministratorDto.class))
                .collect(Collectors.toList()));
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    public ResponseEntity<AdministratorDto> read(@PathVariable int id) throws NoSuchEntityException {
        Optional<Administrator> existingEntity = service.findAdministratorById(id);

        if(!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Administrator admin = existingEntity.get();

        if(Objects.nonNull(admin)) {

            AdministratorDto administratorDto = modelMapper.map(admin, AdministratorDto.class);
            return ResponseEntity.ok(administratorDto);

        }else {
            throw new NoSuchEntityException();
        }

    }
    @RequestMapping(method = RequestMethod.GET, value = "byname/{name}")
    public ResponseEntity<AdministratorDto> read(@PathVariable String name) throws NoSuchEntityException {
        Optional<Administrator> existingEntity = service.findAdministratorByName(name);

        if(!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Administrator admin = existingEntity.get();

        if(Objects.nonNull(admin)) {

            AdministratorDto administratorDto = modelMapper.map(admin, AdministratorDto.class);
            return ResponseEntity.ok(administratorDto);

        }else {
            throw new NoSuchEntityException();
        }

    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AdministratorDto> create(@RequestBody AdministratorDto requestDto) {

        try {
            Administrator requestEntity = modelMapper.map(requestDto, Administrator.class);
            Administrator saveResult = service.save(requestEntity);
            AdministratorDto responseDto = modelMapper.map(saveResult, AdministratorDto.class);

            return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
        } catch (Exception e2) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(method = RequestMethod.PUT, value = "{id}")
    public ResponseEntity<AdministratorDto> update(@PathVariable("id") int id, @RequestBody AdministratorDto requestDto) {
        Optional<Administrator> existingEntity = service.findAdministratorById(id);
        if (!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Administrator requestEntity = modelMapper.map(requestDto, Administrator.class);

        Administrator entity = existingEntity.get();
        entity.setName(requestEntity.getName());
        Administrator saveResult = service.save(entity);

        AdministratorDto responseDto = modelMapper.map(saveResult, AdministratorDto.class);

        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }



     @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<Administrator> delete(@PathVariable int id) {
        Optional<Administrator> existingEntity = service.findAdministratorById(id);
        if(!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try{
            service.delete(existingEntity.get());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
     }

}
