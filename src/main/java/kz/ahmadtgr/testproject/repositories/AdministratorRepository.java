package kz.ahmadtgr.testproject.repositories;

import kz.ahmadtgr.testproject.entities.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ahmad on 09.04.2019.
 */
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

    Optional<Administrator> findByName(String name);

}
