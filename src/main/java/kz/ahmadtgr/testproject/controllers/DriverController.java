package kz.ahmadtgr.testproject.controllers;

import jdk.nashorn.internal.runtime.logging.Logger;
import kz.ahmadtgr.testproject.dtos.DriverDto;
import kz.ahmadtgr.testproject.entities.Driver;
import kz.ahmadtgr.testproject.services.DataAccessService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Ahmad on 11.04.2019.
 */
@Logger
@Slf4j
@RestController
@RequestMapping("/driver")
public class DriverController {

    @Autowired
    DataAccessService service;

    private ModelMapper modelMapper = new ModelMapper();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<DriverDto>> readAll() {
        List<Driver> existingEntities = service.findAllDrivers();

        return ResponseEntity.ok(existingEntities
                .stream()
                .map(driver ->  modelMapper.map(driver, DriverDto.class))
                .collect(Collectors.toList()));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<DriverDto> create(@RequestBody DriverDto requestDto) {

        try {
            Driver requestEntity = modelMapper.map(requestDto, Driver.class);
            Driver saveResult = service.save(requestEntity);
            DriverDto responseDto = modelMapper.map(saveResult, DriverDto.class);

            return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
        } catch (Exception e2) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(method = RequestMethod.PUT, value = "{id}")
    public ResponseEntity<DriverDto> update(@PathVariable("id") int id, @RequestBody DriverDto requestDto) {
        Optional<Driver> existingEntity = service.findDriverById(id);
        if (!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Driver requestEntity = modelMapper.map(requestDto, Driver.class);

        Driver entity = existingEntity.get();
        entity.setName(requestEntity.getName());
        entity.setAdministrator(requestEntity.getAdministrator());
        entity.setBus(requestEntity.getBus());
        Driver saveResult = service.save(entity);

        DriverDto responseDto = modelMapper.map(saveResult, DriverDto.class);

        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<Driver> delete(@PathVariable int id) {
        Optional<Driver> existingEntity = service.findDriverById(id);
        if(!existingEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try{
            service.delete(existingEntity.get());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
