package kz.ahmadtgr.testproject.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Ahmad on 08.04.2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Builder
@Entity
@Table(name = "buses")
public class Bus {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private int id;

    @Column(name = "bus_number")
    private String busNumber;

    @OneToMany(mappedBy = "bus", fetch = FetchType.LAZY)
    private Set<Driver> drivers;

}
