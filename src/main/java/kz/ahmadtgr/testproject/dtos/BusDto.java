package kz.ahmadtgr.testproject.dtos;

import lombok.*;

/**
 * Created by Ahmad on 09.04.2019.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class BusDto {

    private int id;
    private String busNumber;
}
